#include "tester.h"

#define TESTS_COUNT 5

void test_memory_allocation(){
    printf("Test 1: memory allocation\n");
    void *heap = heap_init(REGION_MIN_SIZE);
    if(!heap){
        fprintf(stderr,"%s","error heap");
        return;
    }
    void *malloc = _malloc(REGION_MIN_SIZE);

    if (!malloc){
        fprintf(stderr,"%s" , "error malloc");
        return;
    }
    _free(malloc);
    munmap(HEAP_START,4096);
    printf("test 1 have been completed\n");
}
void test_free_one_block(){
    printf("Test 2: Freeing one blocks from the allocated ones\n");
    void *heap = heap_init(REGION_MIN_SIZE);
    if(!heap){
        fprintf(stderr,"%s","error heap");
        return;
    }
    void *malloc1 = _malloc(REGION_MIN_SIZE);
    void *malloc2 = _malloc(REGION_MIN_SIZE);
    void *malloc3 = _malloc(REGION_MIN_SIZE);
    if(!malloc1 || !malloc2 || !malloc3){
        fprintf(stderr,"%s","error malloc");
        return;
    }
    debug_heap(stdout, heap);
    _free(malloc3);
    debug_heap(stdout, heap);
    _free(malloc2);
    _free(malloc1);
    munmap(HEAP_START, 4096);
    printf("test 2 have been completed\n");
}
void test_free_two_block(){
    printf("Test 3: Freeing two blocks from the allocated ones\n");
    void *heap = heap_init(REGION_MIN_SIZE);
    if(!heap){
        fprintf(stderr,"%s","error heap");
        return;
    }
    void *malloc1 = _malloc(REGION_MIN_SIZE);
    void *malloc2 = _malloc(REGION_MIN_SIZE);
    void *malloc3 = _malloc(REGION_MIN_SIZE);
    void *malloc4 = _malloc(REGION_MIN_SIZE);
    void *malloc5 = _malloc(REGION_MIN_SIZE);
    if(!malloc1 || !malloc2 || !malloc3 || !malloc4 || !malloc5){
        fprintf(stderr,"%s","error malloc");
        return;
    }
    debug_heap(stdout,heap);
    _free(malloc1);
    _free(malloc2);
    debug_heap(stdout,heap);
    _free(malloc3);
    _free(malloc4);
    _free(malloc5);
    munmap(HEAP_START, 4096);
    printf("test 3 have been completed\n");
}
void test_out_of_memory_new_region_extends_old(){
    printf("Test 4: Memory is over, the new memory region expands the old one\n");
    void *heap = heap_init(REGION_MIN_SIZE);
    if(!heap){
        fprintf(stderr,"%s","error heap");
        return;
    }
    void *malloc = _malloc((REGION_MIN_SIZE + 20));
    if(!malloc){
        fprintf(stderr,"%s","error malloc");
        return;
    }
    debug_heap(stdout, heap);
    _free(malloc);
    munmap(HEAP_START, 16384);
    printf("test 4 have been completed\n");
}
void test_out_of_memory_new_region_does_not_extend_old(){
    printf("Test 5: Memory has run out, "
           "the old memory region cannot be expanded due to a different allocated address range,"
           "the new region is allocated in a different location\n");
    void *heap = heap_init(REGION_MIN_SIZE);
    if(!heap){
        printf("error heap\n");
        return;
    }
    struct block_header *bh = (struct block_header *) heap;
    void *malloc1 = _malloc(bh->capacity.bytes);
    debug_heap(stdout, heap);
    void *map = mmap(bh->contents + bh->capacity.bytes, REGION_MIN_SIZE*2, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0);
    void *malloc2 = _malloc(REGION_MIN_SIZE);
    debug_heap(stdout, heap);
    _free(malloc1);
    _free(malloc2);
    munmap(map, size_from_capacity((block_capacity) {.bytes=5000}).bytes);
    munmap(heap, size_from_capacity((block_capacity) {.bytes=REGION_MIN_SIZE}).bytes);
    printf("test 5, have been completed\n");

}

test tests[] = {
        test_free_one_block,
        test_memory_allocation,
        test_free_two_block,
        test_out_of_memory_new_region_extends_old,
        test_out_of_memory_new_region_does_not_extend_old
};


void tester_run() {
    for (size_t i = 0; i < TESTS_COUNT; ++i) {
        tests[i]();
    }
}
