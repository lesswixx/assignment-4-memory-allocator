#ifndef ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTER_H
#define ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTER_H

#include "../mem.h"
#include "../mem_internals.h"
#include <inttypes.h>

void tester_run();
typedef void (*test)();

#endif
